﻿using System;

namespace Text
{
    class Program
    {
        static void Main(string[] args)
        {  //打印乘法表
            for (int i = 1; i < 10; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("{0}x{1}={2}\t",j,i,j*i);
                }
                Console.WriteLine("");
            }

            //打印菱形
            Console.WriteLine("请输入菱形层数");  
            int a = int.Parse(Console.ReadLine());
            if (a%2==0)
            {
                for (int i = 1; i <= a / 2; i++)
                {
                    for (int j = 0; j <= (a / 2) - i; j++)
                    {
                        Console.Write(" ");
                    }
                    for (int k = 1; k <= 2 * i - 1; k++)
                    {
                        Console.Write("*");
                    }
                    Console.WriteLine("");      //打印上半部分
                }
                for (int i = a / 2; i >= 1; i--)
                {
                    for (int j = 0; j <= (a / 2) - i; j++)
                    {
                        Console.Write(" ");
                    }
                    for (int k = 1; k <= 2 * i - 1; k++)
                    {
                        Console.Write("*");
                    }
                    Console.WriteLine("");   //打印下半部分
                }
            }
            else
            {
                Console.WriteLine("请输入一个偶数，谢谢");
            }

           
        }
    }
}
